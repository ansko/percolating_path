import json
import os
import resource
import subprocess
import shutil
import time

from config import fem_libs_directory, fem_exes_directory

from analyze_percolation import analyze_percolation
from distribute_spheres import distribute_spheres_randomly
from write_csg import write_csg


def run_fem():
    ...

def loop(system_creation_method, **kwargs):
    data_out_fname = kwargs['data_out_fname']
    produced_files = []

    new_geo_fname = 'test.geo'
    r = kwargs['r']
    tau = kwargs['tau']

    # make .geo
    csg_system = system_creation_method(**kwargs)
    perc_data = analyze_percolation(csg_system)
    write_csg(csg_system, new_geo_fname, maxh_filler=r/3, maxh_interface=r*tau/3)
    real_N = len(csg_system.fillers)

    # fem
    # configure fem

    so_symlinks = {
        'libssl.so.6': '{0}/openssl-0.9.8e/libssl.so.0.9.8'.format(
            fem_libs_directory),
        'libcrypto.so.6': '{0}/openssl-0.9.8e/libcrypto.so.0.9.8'.format(
            fem_libs_directory),
        'libcurl.so.3': '/usr/lib/x86_64-linux-gnu/libcurl.so.4'.format(
            fem_libs_directory),
        'libmpi_f90.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/f90/.libs/'
            'libmpi_f90.so.0').format(
            fem_libs_directory),
        'libmpi_f77.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/f77/.libs/'
            'libmpi_f77.so.0').format(
            fem_libs_directory),
        'libmpi_cxx.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/cxx/.libs/'
            'libmpi_cxx.so.0').format(
            fem_libs_directory)
    }
    for k, v in so_symlinks.items():
        if not os.path.isfile(k):
            os.symlink(v, k)

    fem_env = os.environ
    fem_env['LD_LIBRARY_PATH'] = '{0}/libs:.'.format(fem_libs_directory)
    gen_mesh_exe = '{0}/gen_mesh.x'.format(fem_exes_directory)
    process_mesh_exe = '{0}/processMesh.x'.format(fem_exes_directory)
    fem_main_exe = '{0}/FEManton3_2019_01_31.o'.format(fem_exes_directory)

    fem_gen_stdout = 'gen_mesh_stdout_{0}'.format(real_N)
    fem_gen_stderr = 'gen_mesh_stderr_{0}'.format(real_N)

    code = subprocess.call([gen_mesh_exe, new_geo_fname, '0.15', '2', '2'],
        env=fem_env,
        stdout=open(fem_gen_stdout, 'w'), stderr=open(fem_gen_stderr, 'w'))
    if code != 0:
        print('gen_mesh returned not 0!', code)
        return
    else:
        produced_files.append(fem_gen_stdout)
        produced_files.append(fem_gen_stderr)
    #if verbose:
    #    print('gen_mesh finished with code', code)
    mesh_size = round(float(os.path.getsize('generated.vol')/2**20), 2)
    print('mesh size ~~ {0} MB'.format(mesh_size))
    shutil.move('generated.vol', 'out.mesh')
    produced_files.append('out.mesh')
    mem = 0.3 * 2**33  # 30% of my 8GB RAM
    fem_process_stdout = 'process_mesh_stdout_{0}'.format(real_N)
    fem_process_stderr = 'process_mesh_stderr_{0}'.format(real_N)
    code = subprocess.call(process_mesh_exe,
        preexec_fn=lambda: resource.setrlimit(resource.RLIMIT_AS, (mem, mem)),
        env=fem_env,
        stdout=open(fem_process_stdout, 'w'), stderr=open(fem_process_stderr, 'w'))
    if code not in [0, 2]:
        print('process_mesh returned bad code!', code)
        return
    else:
        produced_files.append(fem_process_stdout)
        produced_files.append(fem_process_stderr)
    produced_files.append('mesh.xdr')
    #if verbose:
    #    print('processMesh finished with code', code)
    strains = {
        'x': '0.01 0. 0.\n0. 0. 0.\n0. 0. 0.',
        'y': '0. 0. 0.\n0. 0.01 0.\n0. 0. 0.',
        'z': '0. 0. 0.\n0. 0. 0.\n0. 0. 0.01'
    }
    E_idcs = {
        'x': [0, 9],  # [eps, sig]:  E = sig/eps * coeff
        'y': [4, 13],
        'z': [8, 17]
    }
    print('\t {0}/{1}'.format(real_N, kwargs['N']))
    for axis in 'xyz':
        fem_main_stdout = 'fem_main_stdout_{0}_{1}'.format(real_N, axis)
        fem_main_stderr = 'fem_main_stderr_{0}_{1}'.format(real_N, axis)
        input_fname = 'input_{0}{0}'.format(axis)
        produced_files.append(input_fname)
        with open(input_fname, 'w') as f:
            print('SizeX {0}'.format(kwargs['xhi'] - kwargs['xlo']), file=f)
            print('SizeY {0}'.format(kwargs['yhi'] - kwargs['ylo']), file=f)
            print('SizeZ {0}'.format(kwargs['zhi'] - kwargs['zlo']), file=f)
            print('MeshFileName mesh.xdr', file=f)
            print('MaterialsGlobalFileName materials.bin', file=f)
            print('TaskName uniaxial_{0}{0}'.format(axis), file=f)
            print('G_filler {0}'.format(kwargs['Ef']), file=f)
            print('G_interface {0}'.format(kwargs['Ei']), file=f)
            print('G_matrix {0}'.format(kwargs['Em']), file=f)
            print('Strain', file=f)
            print(strains[axis], file=f)
        code = subprocess.call([fem_main_exe, input_fname],
            stdout=open(fem_main_stdout, 'w'), stderr=open(fem_main_stderr, 'w'))
        if code != 0:
            print('fem_main along {0} returned not 0!'.format(axis), code)
            return
        else:
            produced_files.append(fem_main_stdout)
            produced_files.append(fem_main_stderr)
        results_fname = 'uniaxial_{0}{0}_results.txt'.format(axis)
        produced_files.append(results_fname)
        produced_files.append('uniaxial_{0}{0}_log.txt'.format(axis))
        lines = open(results_fname).readlines()
        vol_f, vol, vol_i = lines[1].split()
        fi_f_femmain = float(vol_f) / float(vol)
        fi_i_femmain = float(vol_i) / float(vol)
        #if verbose:
        #    print('fi_f_femmain', fi_f_femmain)
        #    print('fi_i_femmain', fi_i_femmain)
        eps = float(lines[14].split()[E_idcs[axis][0]])
        sig = float(lines[14].split()[E_idcs[axis][1]])
        try:
            E = sig / eps
        except ZeroDivisionError:
            E = 0
        print('\t', fi_f_femmain, axis, E, time.asctime())
        data_entry = {
            'tau': tau,
            'Lx': kwargs['xhi'] - kwargs['xlo'],
            'Lr': (kwargs['xhi'] - kwargs['xlo'])/r,
            'N': N,
            'real_N': real_N,
            'fi': fi_f_femmain,
            'fi_f': fi_f_femmain,
            'fi_i': fi_i_femmain,
            'axis': axis,
            'E': E,
            'Ef': kwargs['Ef'],
            'Ei': kwargs['Ei'],
            'Em': kwargs['Em'],
            'mesh_size': mesh_size,
            'percolation_info': {}
        }
        if perc_data:
            data_entry['clusters_count'] = len(perc_data)
            data_entry['percolation_info'] = []
            for k in sorted(perc_data.keys()):
                v = perc_data[k]
                data_entry['percolation_info'].append({
                    'cluster_id': k,
                    'size': len(v['cluster']),
                    'cluster_perc_x': v['perc_x'],
                    'cluster_perc_y': v['perc_y'],
                    'cluster_perc_z': v['perc_z'],
                    'cluster_lx': v['lx'],
                    'cluster_ly': v['ly'],
                    'cluster_lz': v['lz'],
                    'cluster_r_gyr_xx': v['r_gyr_xx'],
                    'cluster_r_gyr_yy': v['r_gyr_yy'],
                    'cluster_r_gyr_zz': v['r_gyr_zz'],
                })
        #print(data_entry)
        #for idx in range(
        #if perc_data:
        #    data_entry['percolation'] = perc_data['perc_{0}'.format(axis)]
        if 'maxh_filler' in kwargs.keys():
            data_entry['maxh_filler'] = kwargs['maxh_filler']
        else:
            data_entry['maxh_filler'] = False
        if 'maxh_interface' in kwargs.keys():
            data_entry['maxh_interface'] = kwargs['maxh_interface']
        else:
            data_entry['maxh_interface'] = False
        #if shape == 'disks':
        #    data_entry['ar'] = ar
        #    data_entry['R'] = R
        #    data_entry['shape'] = 'disks'
        #    data_entry['percolation'] = perc[axis]
        #    data_entry['vertices_number'] = vertices_number
        #elif shape == 'spheres':
        #    data_entry['shape'] = 'spheres'
        #    data_entry['r'] = r
        data_entry['shape'] = 'spheres'
        data_entry['r'] = r
        #if verbose:
        #    print(data_entry)
        if data_out_fname in os.listdir():
            results_ready = json.load(open(data_out_fname))
        else:
            results_ready = []
        results_ready.append(data_entry)
        with open(data_out_fname, 'w') as f:
            json.dump(results_ready, f, indent=4)

        try:
            if fi_f_femmain > kwargs['fi_f_threshold']:
                print('fi_f threshold reached, stopping')
                sys.exit()
        except KeyError:
            pass
        try:
            if real_N > kwargs['N_threshold']:
                print('N threshold reached, stopping')
                sys.exit()
        except KeyError:
            pass
    for fname in produced_files:
        os.remove(fname)


if __name__ == '__main__':
    Lr = 50
    tau = 1

    r = 1
    L = r * Lr

    for N in range(1, 10):
        loop(distribute_spheres_randomly, N=N, r=r, xlo=0, ylo=0, zlo=0,
            xhi=L, yhi=L, zhi=L, tau=tau, Ef=232, Em=1.5, Ei=5,
            data_out_fname='test.json')
