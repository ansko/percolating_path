import matplotlib
import matplotlib.pyplot as plt
import random


def plot():
    lines = open('log').readlines()
    xs = [float(line.split()[0]) for line in lines]
    ys = [float(line.split()[1]) for line in lines]
    fig = plt.figure()
    for idx in range(len(xs)):
        color = 'r' if ys[idx] == 0 else 'b'
        #plt.plot(xs[idx], random.random(), color=color, marker='o')
        plt.plot(xs[idx], ys[idx], color=color, marker='o')
    #plt.xlim([0.15, 0.3])
    fig.savefig('pic.png')
    return 0


if __name__ == '__main__':
    plot()
