def write_csg(csg_system, fname,
        maxh_filler=None, maxh_interface=None, maxh_matrix=None):

    if maxh_filler is not None:
        maxh_filler_specifier = ' -maxh={0}'.format(maxh_filler)
    else:
        maxh_filler_specifier = ''
    if maxh_interface is not None:
        maxh_interface_specifier = ' -maxh={0}'.format(maxh_interface)
    else:
        maxh_interface_specifier = ''
    if maxh_matrix is not None:
        maxh_matrix_specifier = ' -maxh={0}'.format(maxh_matrix)
    else:
        maxh_matrix_specifier = ''

    with open(fname, 'w') as f:
        print('algebraic3d\n', file=f)
        print('solid cell = {0};\n'.format(csg_system.cell), file=f)
        for idx, filler in enumerate(csg_system.fillers):
            print('solid filler_{0} = {1};'.format(idx, filler), file=f)
        print('', file=f)
        for idx, interface in enumerate(csg_system.interfaces):
            print('solid interface_{0} = {1} and cell;'.format(idx, interface),
                file=f)
        print('', file=f)

        print('solid filler = {0};\n'.format(' or '.join(
            ['filler_{0}'.format(idx) for idx in range(len(csg_system.fillers))])),
            file=f)
        if csg_system.interfaces:
            print('solid interface = {0}'.format(' or '.join(
                ['interface_{0}'.format(idx)
                    for idx in range(len(csg_system.interfaces))])),
                'and not filler;', file=f)
        print('tlo filler{0};\n'.format(maxh_filler_specifier), file=f)
        if csg_system.interfaces:
            print('tlo interface -transparent{0};\n'.format(
                maxh_interface_specifier), file=f)
            print('solid matrix = not filler and not interface and cell;\n', file=f)
        else:
            print('solid matrix = not filler and cell;\n', file=f)
        print('tlo matrix -transparent{0};'.format(maxh_matrix_specifier), file=f)
