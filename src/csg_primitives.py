#http://netgen-mesher.sourceforge.net/docs/ng4.pdf


class CSGOrthobrick:
    def __init__(self, xlo, ylo, zlo, xhi, yhi, zhi):
        self.xlo = xlo
        self.ylo = ylo
        self.zlo = zlo
        self.xhi = xhi
        self.yhi = yhi
        self.zhi = zhi

    def __str__(self):
        return 'orthobrick({0}, {1}, {2}; {3}, {4}, {5})'.format(
            self.xlo, self.ylo, self.zlo, self.xhi, self.yhi, self.zhi)


class CSGSphere:
    def __init__(self, x, y, z, r):
        self.primitive_type = 'sphere'
        self.x = x
        self.y = y
        self.z = z
        self.r = r

    def __str__(self):
        return 'sphere({0}, {1}, {2}; {3})'.format(
            self.x, self.y, self.z, self.r)
