import json
import os
import resource
import subprocess
import shutil
import time

from analyze_percolation import analyze_percolation
from distribute_spheres import distribute_spheres_randomly
from write_csg import write_csg

from config import (fem_libs_directory, fem_exes_directory)


def perform_fem(**kwargs):
    def make_symlinks():
        so_symlinks = {
            'libssl.so.6': '{0}/openssl-0.9.8e/libssl.so.0.9.8'.format(
                fem_libs_directory),
            'libcrypto.so.6': '{0}/openssl-0.9.8e/libcrypto.so.0.9.8'.format(
                fem_libs_directory),
            'libcurl.so.3': '/usr/lib/x86_64-linux-gnu/libcurl.so.4'.format(
                fem_libs_directory),
            'libmpi_f90.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/f90/.libs/'
                'libmpi_f90.so.0').format(
                fem_libs_directory),
            'libmpi_f77.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/f77/.libs/'
                'libmpi_f77.so.0').format(
                fem_libs_directory),
            'libmpi_cxx.so.0': ('{0}/openmpi-1.4.5/build/ompi/mpi/cxx/.libs/'
                'libmpi_cxx.so.0').format(
                fem_libs_directory)
        }
        for k, v in so_symlinks.items():
            if not os.path.isfile(k):
                os.symlink(v, k)

    def write_input(**kwargs):
        with open(kwargs['input_fname'], 'w') as f:
            print('SizeX {0}'.format(kwargs['xhi'] - kwargs['xlo']), file=f)
            print('SizeY {0}'.format(kwargs['yhi'] - kwargs['ylo']), file=f)
            print('SizeZ {0}'.format(kwargs['zhi'] - kwargs['zlo']), file=f)
            print('MeshFileName mesh.xdr', file=f)
            print('MaterialsGlobalFileName materials.bin', file=f)
            print('TaskName uniaxial_{0}{0}'.format(axis), file=f)
            print('G_filler {0}'.format(kwargs['Ef']), file=f)
            print('G_interface {0}'.format(kwargs['Ei']), file=f)
            print('G_matrix {0}'.format(kwargs['Em']), file=f)
            print('Strain', file=f)
            print(strains[axis], file=f)

    produced_files = []
    fem_env = os.environ
    fem_env['LD_LIBRARY_PATH'] = '{0}/libs:.'.format(fem_libs_directory)
    gen_mesh_exe = '{0}/gen_mesh.x'.format(fem_exes_directory)
    process_mesh_exe = '{0}/processMesh.x'.format(fem_exes_directory)
    fem_main_exe = '{0}/FEManton3_2019_01_31.o'.format(fem_exes_directory)
    fem_gen_stdout = 'gen_mesh_stdout_{0}'.format(kwargs['real_N'])
    fem_gen_stderr = 'gen_mesh_stderr_{0}'.format(kwargs['real_N'])
    fem_process_stdout = 'process_mesh_stdout_{0}'.format(kwargs['real_N'])
    fem_process_stderr = 'process_mesh_stderr_{0}'.format(kwargs['real_N'])
    gen_mesh_args = [kwargs['geo_fname'], '0.15', '2', '2']
    generated_data = {}
    produced_files.append(fem_gen_stdout)
    produced_files.append(fem_gen_stderr)
    produced_files.append(fem_process_stdout)
    produced_files.append(fem_process_stderr)

    code = subprocess.call([gen_mesh_exe, *gen_mesh_args],
        env=fem_env,
        stdout=open(fem_gen_stdout, 'w'), stderr=open(fem_gen_stderr, 'w'))
    if code != 0:
        print('gen_mesh returned not 0!', code)
        return
    else:
        produced_files.append(fem_gen_stdout)
        produced_files.append(fem_gen_stderr)

    mesh_size = round(float(os.path.getsize('generated.vol')/2**20), 2)
    print('mesh size ~~ {0} MB'.format(mesh_size))
    generated_data['mesh_size'] = mesh_size
    shutil.move('generated.vol', 'out.mesh')
    produced_files.append('out.mesh')

    mem = 0.3 * 2**33  # 30% of my 8GB RAM
    code = subprocess.call(process_mesh_exe,
        preexec_fn=lambda: resource.setrlimit(resource.RLIMIT_AS, (mem, mem)),
        env=fem_env,
        stdout=open(fem_process_stdout, 'w'), stderr=open(fem_process_stderr, 'w'))
    if code not in [0, 2]:
        print('process_mesh returned bad code!', code)
        return
    else:
        produced_files.append(fem_process_stdout)
        produced_files.append(fem_process_stderr)
    produced_files.append('mesh.xdr')

    strains = {
        'x': '0.01 0. 0.\n0. 0. 0.\n0. 0. 0.',
        'y': '0. 0. 0.\n0. 0.01 0.\n0. 0. 0.',
        'z': '0. 0. 0.\n0. 0. 0.\n0. 0. 0.01'
    }
    E_idcs = {
        'x': [0, 9],  # [eps, sig]:  E = sig/eps * coeff
        'y': [4, 13],
        'z': [8, 17]
    }
    for axis in kwargs['axis']:
        fem_main_stdout = 'fem_main_stdout_{0}_{1}'.format(kwargs['real_N'], axis)
        fem_main_stderr = 'fem_main_stderr_{0}_{1}'.format(kwargs['real_N'], axis)
        input_fname = 'input_{0}{0}'.format(axis)
        produced_files.append(input_fname)
        produced_files.append(fem_main_stdout)
        produced_files.append(fem_main_stderr)
        write_input(input_fname=input_fname, **kwargs)
        code = subprocess.call([fem_main_exe, input_fname],
            stdout=open(fem_main_stdout, 'w'), stderr=open(fem_main_stderr, 'w'))
        if code != 0:
            print('fem_main along {0} returned not 0!'.format(axis), code)
            return
        else:
            produced_files.append(fem_main_stdout)
            produced_files.append(fem_main_stderr)
        results_fname = 'uniaxial_{0}{0}_results.txt'.format(axis)
        #produced_files.append(results_fname)
        produced_files.append('uniaxial_{0}{0}_log.txt'.format(axis))
        lines = open(results_fname).readlines()
        vol_f, vol, vol_i = lines[1].split()
        fi_f_femmain = float(vol_f) / float(vol)
        fi_i_femmain = float(vol_i) / float(vol)
        eps = float(lines[14].split()[E_idcs[axis][0]])
        sig = float(lines[14].split()[E_idcs[axis][1]])
        try:
            E = sig / eps
        except ZeroDivisionError:
            E = 0
        print('\t', fi_f_femmain, axis, E, time.asctime())
        generated_data[axis] = {
            'E': E,
            'fi': fi_f_femmain,
            'fi_i': fi_i_femmain
        }
    generated_data['produced_files'] = produced_files
    return generated_data


def loop(system_creation_method, analyzers=[], **kwargs):
    data_out_fname = kwargs['data_out_fname']
    data_entry = {
        'tau': kwargs['tau'],
        'Lx': kwargs['xhi'] - kwargs['xlo'],
        'Lr': (kwargs['xhi'] - kwargs['xlo']) / kwargs['r'],
        'N': kwargs['N'],
        'Ef': kwargs['Ef'],
        'Ei': kwargs['Ei'],
        'Em': kwargs['Em'],
    }

    # make .geo
    csg_system = system_creation_method(**kwargs)
    geo_fname = '{0}.geo'.format(len(csg_system.fillers))
    write_csg(csg_system, geo_fname, maxh_filler=r/3, maxh_interface=r*tau/3)
    real_N = len(csg_system.fillers)

    print('\t {0}/{1}'.format(kwargs['N'], real_N))

    for analyzer in analyzers:
        analyzer_data = analyzer(csg_system)
        clusters = []
        for k, v in analyzer_data.items():
            clusters.append({
                'count': len(v['cluster']),
                'perc_x': v['perc_x'],
                'perc_y': v['perc_y'],
                'perc_z': v['perc_z'],
                'lx': v['lx'],
                'ly': v['ly'],
                'lz': v['lz'],
                'r_gyr_xx': v['r_gyr_xx'],
                'r_gyr_yy': v['r_gyr_yy'],
                'r_gyr_zz': v['r_gyr_zz']
            })
        #print(clusters)
        #print('---------')
        #print(clusters)
        data_entry['clusters'] = clusters
        #print('---------')

    # perform fem
    fem_kwargs = {
        'geo_fname': geo_fname,
        'axis': 'xyz',
        'real_N': real_N,
        **kwargs
    }
    fem_data = perform_fem(**fem_kwargs)

    # clean up
    for fname in fem_data['produced_files']:
        try:
            os.remove(fname)
        except FileNotFoundError:
            pass
    del fem_data['produced_files']

    # contstruct data entries:
    for axis in fem_kwargs['axis']:
        new_data_entry = data_entry
        new_data_entry['mesh_size'] = fem_data['mesh_size']
        new_data_entry['axis'] = axis
        new_data_entry['E'] = fem_data[axis]['E']
        new_data_entry['fi'] = fem_data[axis]['fi']
        new_data_entry['fi_i'] = fem_data[axis]['fi_i']
        #print(new_data_entry)
        if data_out_fname in os.listdir():
            old_data = json.load(open(data_out_fname))
        else:
            old_data = []
        old_data.append(new_data_entry)
        json.dump(old_data, open(data_out_fname, 'w'), indent=4)

    #print(fem_data)


if __name__ == '__main__':
    Lr = 50
    tau = 1

    r = 1
    L = r * Lr

    kwargs = {
        'r': r,
        'xlo': 0, 'ylo': 0, 'zlo': 0, 'xhi': L, 'yhi': L, 'zhi': L,
        'tau': tau,
        'Ef': 232, 'Em': 1.5, 'Ei': 5,
        'data_out_fname': 'test.json'
    }

    for N in range(18, 101):
        loop(distribute_spheres_randomly, analyzers=[analyze_percolation],
             **kwargs, N=N)
