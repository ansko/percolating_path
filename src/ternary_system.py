from csg_primitives import CSGOrthobrick


class TernarySystem:
    def __init__(self, xlo, xhi, ylo, yhi, zlo, zhi):
        self.cell = CSGOrthobrick(xlo, ylo, zlo, zhi, yhi, zhi)
        self.fillers = []
        self.interfaces = []

