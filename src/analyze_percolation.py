def analyze_percolation(csg_system):
    def crosses_to_clasters(crosses, N):
        clusters = []
        for cross in crosses:
            is_found = False
            for particle in cross:
                for cluster in clusters:
                    if particle in cluster:
                        cluster.update(cross)
                        is_found = True
            if not is_found:
                clusters.append(set(cross))
        while sum([len(cluster) for cluster in clusters]) > N:
            delete_idx = None
            for id_i, cluster_i in enumerate(clusters):
                for idx_j, cluster_j in enumerate(clusters):
                    if cluster_i is cluster_j:
                        continue
                    if cluster_i.intersection(cluster_j):
                        cluster_i.update(cluster_j)
                        delete_idx = idx_j
                        break
                if delete_idx is not None:
                    break
            clusters.pop(delete_idx)
        return clusters

    xlo = csg_system.cell.xlo
    xhi = csg_system.cell.xhi
    ylo = csg_system.cell.ylo
    yhi = csg_system.cell.yhi
    zlo = csg_system.cell.zlo
    zhi = csg_system.cell.zhi
    crosses = set()
    for idx_i, interface_i in enumerate(csg_system.interfaces):
        for idx_j, interface_j in enumerate(csg_system.interfaces):
            if interface_i is interface_j:
                continue
            dx = interface_j.x - interface_i.x
            dy = interface_j.y - interface_i.y
            dz = interface_j.z - interface_i.z
            dr = interface_j.r + interface_i.r
            if dr**2 > dx**2 + dy**2 + dz**2:
                crosses.add((min(idx_i, idx_j), max(idx_i, idx_j)))
    clusters = crosses_to_clasters(crosses, len(csg_system.interfaces))
    data = {}
    for idx, cluster in enumerate(clusters):
        x_cm = y_cm = z_cm = 0
        min_x = xhi
        max_x = xlo
        min_y = yhi
        max_y = ylo
        min_z = zhi
        max_z = zlo
        for particle_idx in cluster:
            particle = csg_system.interfaces[particle_idx]
            x_cm += particle.x
            y_cm += particle.y
            z_cm += particle.z
            min_x = min(min_x, particle.x - particle.r)
            min_y = min(min_y, particle.y - particle.r)
            min_z = min(min_z, particle.z - particle.r)
            max_x = max(max_x, particle.x + particle.r)
            max_y = max(max_y, particle.y + particle.r)
            max_z = max(max_z, particle.z + particle.r)
        x_cm /= len(cluster)
        y_cm /= len(cluster)
        z_cm /= len(cluster)
        r_gyr_xx = r_gyr_yy = r_gyr_zz = 0
        for particle_idx in cluster:
            particle = csg_system.interfaces[particle_idx]
            r_gyr_xx += (particle.x - x_cm)**2
            r_gyr_yy += (particle.y - y_cm)**2
            r_gyr_zz += (particle.z - z_cm)**2
        data[idx] = {
            'cluster': cluster,
            'perc_x': max_x - min_x > xhi - xlo,
            'lx': min(max_x - min_x, xhi - xlo),
            'perc_y': max_y - min_y > yhi - ylo,
            'ly': min(max_y - min_y, yhi - ylo),
            'perc_z': max_z - min_z > zhi - zlo,
            'lz': min(max_z - min_z, zhi - zlo),
            'r_gyr_xx': (r_gyr_xx / len(cluster))**0.5,
            'r_gyr_yy': (r_gyr_yy / len(cluster))**0.5,
            'r_gyr_zz': (r_gyr_zz / len(cluster))**0.5
        }
    return data
