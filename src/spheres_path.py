#!/usr/bin/env python3


from distribute_spheres import distribute_spheres_randomly
from write_csg import write_csg
from analyze_percolation import analyze_percolation


def main():
    f = open('log', 'w')
    count = 0

    hysto = {float(i)/100: [0, 0] for i in range(0, 100000)}

    while count < 1e3:
        ts = distribute_spheres_randomly(N=100, r=1, xlo=-10, ylo=-10, zlo=-10,
            xhi=10, yhi=10, zhi=10, tau=1)
        N_real = len(ts.fillers)
        fi = N_real * 4/3 * 3.14 * 1 / 8000
        data = analyze_percolation(ts)
        for entry in data.values():
            count += 1
            print(count)
            rho = len(entry['cluster'])
            rx = entry['r_gyr_xx']
            ry = entry['r_gyr_yy']
            rz = entry['r_gyr_zz']
            print(rx, ry, rz, entry['perc_x'], entry['perc_y'], entry['perc_z'])
            key_x = round((ry*rz)**0.5 / rx * rho, 0)
            key_y = round((rx*rz)**0.5 / ry * rho, 0)
            key_z = round((ry*rx)**0.5 / rz * rho, 0)
            hysto[key_x][0] += entry['perc_x']
            hysto[key_x][1] += 1
            hysto[key_y][0] += entry['perc_y']
            hysto[key_y][1] += 1
            hysto[key_z][0] += entry['perc_z']
            hysto[key_z][1] += 1
    with open('log', 'w') as f:
        for k in sorted(hysto.keys()):
            if hysto[k][1] > 0:
                print(k, hysto[k][0] / hysto[k][1], file=f)


if __name__ == '__main__':
    main()
