import random
import sys
import time

from csg_primitives import CSGSphere
from ternary_system import TernarySystem


def distribute_spheres_randomly(**kwargs):
    # Parse passed arguments
    try:
        xlo = kwargs['xlo']
        xhi = kwargs['xhi']
        ylo = kwargs['ylo']
        yhi = kwargs['yhi']
        zlo = kwargs['zlo']
        zhi = kwargs['zhi']
        N = kwargs['N']
        r = kwargs['r']
        tau = kwargs['tau']
    except KeyError:
        print('Not all required arguments passed in module', __name__)
        sys.exit()

    # Start algorithm
    random.seed(int(1000*(time.time())) % 10000)
    ts = TernarySystem(xlo, xhi, ylo, yhi, zlo, zhi)
    N_ready = 0
    fails_done = 0
    fails_allowed = N * 100
    while N_ready < N and fails_done < fails_allowed:
        new_x = xlo + (xhi - xlo) * random.random()
        if not xlo + r < new_x < xhi - r:
            fails_done += 1
            continue
        new_y = ylo + (yhi - ylo) * random.random()
        if not ylo + r < new_y < yhi - r:
            fails_done += 1
            continue
        new_z = zlo + (zhi - zlo) * random.random()
        if not zlo + r < new_z < zhi - r:
            fails_done += 1
            continue
        crosses_with_existing = False
        for filler in ts.fillers:
            dx = filler.x - new_x
            dy = filler.y - new_y
            dz = filler.z - new_z
            dr = filler.r + r
            if dr**2 > dx**2 + dy**2 + dz**2:
                crosses_with_existing = True
                break
        if not crosses_with_existing:
            ts.fillers.append(CSGSphere(r=r, x=new_x, y=new_y, z=new_z))
            ts.interfaces.append(CSGSphere(
                r=r * (1 + tau), x=new_x, y=new_y, z=new_z))
            N_ready += 1
        else:
            fails_done += 1

    return ts
